# Array Problems on Leetcode


### 0. Kadanes Algo

- https://leetcode.com/problems/maximum-subarray/
- https://leetcode.com/problems/maximum-sum-circular-subarray/
- https://leetcode.com/problems/longest-turbulent-subarray/


### 1. Sliding Window Fixed

- https://leetcode.com/problems/contains-duplicate-ii/
- https://leetcode.com/problems/number-of-sub-arrays-of-size-k-and-average-greater-than-or-equal-to-threshold/


### 2. Sliding Window Variable

- https://leetcode.com/problems/minimum-size-subarray-sum/
- https://leetcode.com/problems/longest-substring-without-repeating-characters/
- https://leetcode.com/problems/longest-repeating-character-replacement/

### 3. Two Pointers

- https://leetcode.com/problems/valid-palindrome/
- https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
- https://leetcode.com/problems/remove-duplicates-from-sorted-array/
- https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/
- https://leetcode.com/problems/container-with-most-water/
- https://leetcode.com/problems/trapping-rain-water/

### 4. Prefix Sums

- https://leetcode.com/problems/range-sum-query-immutable/
- https://leetcode.com/problems/range-sum-query-2d-immutable/
- https://leetcode.com/problems/find-pivot-index/
- https://leetcode.com/problems/product-of-array-except-self/
- https://leetcode.com/problems/subarray-sum-equals-k/