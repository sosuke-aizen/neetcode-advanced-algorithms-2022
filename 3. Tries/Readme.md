# Tries Problems on Leetcode


### 1. Trie

- https://leetcode.com/problems/implement-trie-prefix-tree/
- https://leetcode.com/problems/design-add-and-search-words-data-structure/
- https://leetcode.com/problems/word-search-ii/
- https://leetcode.com/problems/prefix-and-suffix-search/


### 2. Union Find

- https://leetcode.com/problems/redundant-connection/
- https://leetcode.com/problems/accounts-merge/
- https://leetcode.com/problems/longest-consecutive-sequence/
- https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/

### 3. Segment Tree

- https://leetcode.com/problems/range-sum-query-mutable/
- https://leetcode.com/problems/queue-reconstruction-by-height/
- https://leetcode.com/problems/my-calendar-i/
- https://leetcode.com/problems/longest-increasing-subsequence-ii/

### 4. Iterative DFS

- https://leetcode.com/problems/binary-search-tree-iterator/
- https://leetcode.com/problems/binary-tree-preorder-traversal/
- https://leetcode.com/problems/binary-tree-postorder-traversal/