# Backtracking Problems on Leetcode

## 1. Subsets

- https://leetcode.com/problems/subsets/
- - Solution: https://youtu.be/REOH22Xwdkk

- https://leetcode.com/problems/subsets-ii/
- - Solution: https://youtu.be/Vn2v6ajA7U0


## 2. Combinations

- https://leetcode.com/problems/combinations/
- - Solution: https://youtu.be/q0s6m7AiM7o

- https://leetcode.com/problems/combination-sum/
- - Solution: https://youtu.be/GBKI9VSKdGg

- https://leetcode.com/problems/letter-combinations-of-a-phone-number/
- - Solution: https://youtu.be/0snEunUacZY


## 3. Permutation

- https://leetcode.com/problems/permutations/
- - Solution: https://youtu.be/s7AvT7cGdSo

- https://leetcode.com/problems/permutations-ii/
- - Solution: https://youtu.be/qhBVWf0YafA 
