# Graph Problems on Leetcode


### 1. Dijkstra

- https://leetcode.com/problems/network-delay-time/
- https://leetcode.com/problems/swim-in-rising-water/
- https://leetcode.com/problems/path-with-maximum-probability/


### 2. Prim_s

- https://leetcode.com/problems/min-cost-to-connect-all-points/
- https://leetcode.com/problems/find-critical-and-pseudo-critical-edges-in-minimum-spanning-tree/

### 3. Kruskal_s

- https://leetcode.com/problems/min-cost-to-connect-all-points/
- https://leetcode.com/problems/find-critical-and-pseudo-critical-edges-in-minimum-spanning-tree/


### 4. Topological Sort

- https://leetcode.com/problems/course-schedule/
- https://leetcode.com/problems/course-schedule-ii/
- https://leetcode.com/problems/course-schedule-iv/
- https://leetcode.com/problems/sort-items-by-groups-respecting-dependencies/
- https://leetcode.com/problems/alien-dictionary/