# Dynamic Programming Problems on Leetcode


### 1. 0-1 Knapsack

- https://leetcode.com/problems/partition-equal-subset-sum/
- https://leetcode.com/problems/target-sum/
- https://leetcode.com/problems/ones-and-zeroes/
- https://leetcode.com/problems/last-stone-weight-ii/


### 2. Unbounded Knapsack

- https://leetcode.com/problems/coin-change/
- https://leetcode.com/problems/coin-change-ii/
- https://leetcode.com/problems/minimum-cost-for-tickets/

### 3. LCS

- https://leetcode.com/problems/longest-common-subsequence/
- https://leetcode.com/problems/distinct-subsequences/
- https://leetcode.com/problems/edit-distance/
- https://leetcode.com/problems/interleaving-string/
- https://leetcode.com/problems/shortest-common-supersequence/

### 4. Palindromes

- https://leetcode.com/problems/longest-palindromic-substring/
- https://leetcode.com/problems/palindromic-substrings/
- https://leetcode.com/problems/longest-palindromic-subsequence/